import json


#open() function opens the JSON file in read mode
with open(r'C:\Users\pkommabathula\Desktop\json crud\raw_data.json','r') as f:


# the file is parsed using json.load() method which gives us a dictionary named data.
# json.load() takes a file object and returns the json object
    data = json.load(f)

       
# accessing all the items(key,value pairs) present in the dictionary
print(data.items())


# accessing only a particular value for the provided key
print(data["students"][0]["name"])

