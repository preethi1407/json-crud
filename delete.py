import json

#open() function opens the JSON file in read mode
with open(r'C:\Users\pkommabathula\Desktop\json crud\raw_data.json','r') as f:


# # the file is parsed using json.load() method which gives us a dictionary named data.
# json.load() takes a file object and returns the json object
    data=json.load(f)

# Iterate through the objects in the JSON and DELETE (remove) the obj once we find it.                                                      
for i in range(len(data)):
    if data["students"][i]["name"] == "preethi":
        del data["students"][i]
        break

# Output the updated file                                     
open("deleted-file.json", "w").write(
    json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
)