import json


#open() function opens the JSON file in read mode
with open(r'C:\Users\pkommabathula\Desktop\json crud\raw_data.json','r') as f:


# the file is parsed using json.load() method which gives us a dictionary named data.
# json.load() takes a file object and returns the json object
    data=json.load(f)


# updating an existing field in the dictionary by accessing the key of the element
data["students"][0]["name"] = "bala"


# adding a new item to an existing dictionary
data["day-scholar"]=True



# opening the file in write mode
with open("updated.json", "w") as jsonFile:


# dump() which converts the Python objects into appropriate json objects
    json.dump(data, jsonFile)