import json

person = {
    "students" : [
        {
            "name" : "preethi",
            "age" : 21, 
            "college" : "AU", 
            "branch" : "ece", 
            "fee-paid" : True
        },

        {
            "name" : "sowmya",
            "age" : 22, 
            "college" : "AU", 
            "branch" : "cse", 
            "fee-paid" : True
        }
    ]
}



# creating a json object here it is a json string from a python dictionary
personJSON = json.dumps(person, indent=4, sort_keys=True, separators = (' ;',' = '))
print(personJSON) 

# creating a new json file 
with open('person.json', 'w') as file:
    json.dump(person, file, indent = 4, sort_keys=True, separators=(' ;',' = '))
